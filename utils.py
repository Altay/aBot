from __future__ import division

import re
import time

from datetime import timedelta

# From http://stackoverflow.com/questions/19053707/converting-snake-case-to-lower-camel-case-lowercamelcase


def snake_to_camel_case(snake_str):
    components = snake_str.split("_")
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return "".join(x.title() for x in components)


def seconds_to_time(seconds):
    """
    >>> seconds_to_time(1)
    '1'
    >>> seconds_to_time(61)
    '1:01'
    >>> seconds_to_time(666)
    '11:06'

    :param seconds:
    :type seconds: Any int representation
    :return: a string representing time
    :rtype: str
    """
    return str(timedelta(seconds=int(seconds))).strip("0:")


def retry(ExceptionType, retries=3, delay=0.1, backoff=2, logger=None):
    def decorated(func):
        def wrapper(*args, **kwargs):
            n_tries = 1
            next_delay = delay
            while n_tries <= retries:
                try:
                    return func(*args, **kwargs)
                except ExceptionType as e:
                    if logger is not None:
                        logger.warning(f"Retrying in {delay}s, try number {n_tries+1}")
                    time.sleep(next_delay)
                    n_tries += 1
                    next_delay *= backoff

        return wrapper

    return decorated


def iso8601_to_duration(iso8601):
    matches = re.findall(r'^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:.\d+)?)S)?$', iso8601)[0]
    matches = filter(bool, matches)
    return ":".join(matches)
