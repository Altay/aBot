import re
import random

from plugin import Plugin, PluginException


class Cloud(Plugin):
    @classmethod
    def populate_list(cls):
        clouds = [
            "Cirrus",
            "Cirrocumulus",
            "Cirrostratus",
            "Altocumulus",
            "Altostratus",
            "Stratocumulus",
            "Stratus",
            "Cumulus",
            "Nimbostratus",
            "Cumulonimbus",
        ]
        return clouds

    def __init__(self):
        help = "Vieux monsieur qui se plaint des nuages."
        name = "cloud"
        Plugin.__init__(self, name, help)
        self.commands = []
        self.regexps = [
            ".*(([Pp]utain)|([Ff]uck)|([Bb]ordel)|([Mm]erde)).*[Cc]loud.*",
            ".*[Cc]loud.*(([Pp]utain)|([Ff]uck)|([Bb]ordel)|([Mm]erde)).*",
        ]
        self.authors = ["Un_pseudo", "Un_pseudal", "Un_pseudo_", "Altay"]
        self.clouds = Cloud.populate_list()

    def call(self, bot, context, *args):
        if context["author"] in self.authors:
            cloud = random.sample(self.clouds, 1)[0]
            answer = "୧( ಠ Д ಠ )୨  {name} !".format(name=cloud)
            bot.connection.privmsg(context["channel"], answer)
