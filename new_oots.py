import feedparser

from plugin import Plugin


class NewOots(Plugin):
    def __init__(self):
        help = "Détecte la publication d'une nouvelle planche de Order of the Stick."
        name = "new_oots"
        Plugin.__init__(self, name, help)
        self.commands = ["oots"]
        # TODO : initialize with sane value
        self.last_element = None

    def call(self, bot, context, *args):
        try:
            # TODO : allow several new elements
            is_new, title, link = self.update_new_elem()
            if is_new:
                answer = "Oui, il y a un nouvel épisode d'Order of the Stick : {} ({})".format(
                    title, link
                )
            else:
                answer = "Non, désolé, pas de nouvel épisode pour l'instant (lien du dernier épisode : {})".format(
                    link
                )
            bot.connection.privmsg(context["channel"], answer)
        except:
            # TODO
            raise

    def update_new_elem(self):
        feed = feedparser.parse(self.config["oots_url"])
        last_entry = feed.entries[0]
        is_new = last_entry["title"] != self.last_element
        if is_new:
            self.last_element = last_entry["title"]
        return (is_new, last_entry["title"], last_entry["link"])
