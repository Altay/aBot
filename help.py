from plugin import Plugin, PluginException


class Help(Plugin):
    """
    Plugin that shows some helpful information on how to use the bot.
    """

    @staticmethod
    def help(bot, context, *args):
        if len(args) > 0:
            plugin_name = args[0]
            plugin = bot.get_plugin(plugin_name)
            if plugin is None:
                answer = "Le module {plugin} n'existe pas.".format(plugin=plugin_name)
            else:
                answer = Help.build_help(plugin)
        else:
            plugins = Help.plugins(bot, context, *args)
            answer = "Moi j'suis aBot, et j'suis là pour aider. Modules chargés : {}".format(
                plugins
            )
        return answer

    @staticmethod
    def version(bot, context, *args):
        answer = "aBot, version 0.0.1-dev"
        return answer

    @staticmethod
    def plugins(bot, context, *args):
        plugins = map(lambda m: m.name, bot.plugins)
        answer = ", ".join(plugins)
        return answer

    @staticmethod
    def build_help(plugin):
        answer = "Module {name} : ".format(name=plugin.name)
        commands = plugin.commands
        if len(commands) > 0:
            answer += "commandes [{}]. ".format(", ".join(commands))
        answer += plugin.help
        return answer

    def __init__(self):
        help = "Affiche de l'information utile."
        name = "help"
        super().__init__(name, help)
        self.functions_ = {
            "help": Help.help,
            "version": Help.version,
            "plugins": Help.plugins,
        }
        self.commands = self.functions_.keys()

    def call(self, bot, context, *args):
        answer = self.functions_[args[0]](bot, context, *args[1:])
        bot.connection.privmsg(context["channel"], answer)
