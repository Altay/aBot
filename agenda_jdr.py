import arrow
import datetime
import dateutil.rrule
import icalendar
import locale
import re
import requests

from icalevents import icalevents
from ics import Calendar, Event
from collections import defaultdict
from pytz import timezone

from plugin import Plugin


locale.setlocale(locale.LC_ALL, "fr_FR.utf8")


class AgendaJdr(Plugin):
    """Plugin "Ce soir on joue ?"
    
    Depuis un calendrier ical distant, récupère la date du prochain événement et l'affiche
    avec son titre. À défaut, l'événement le plus récent est utilisé.
    
    Configuration :
        calendar_url : URL du calendrier ical
    """

    localtz = timezone("Europe/Paris")

    def __init__(self):
        help = "Renvoie la prochaine partie selon l'agenda des JdR."
        name = "agenda_jdr"
        Plugin.__init__(self, name, help)
        self.commands = ["jdr", "cesoironjoue", "cesoironjoue?", "csoj?", "csoj"]

    def call(self, bot, context, *args):
        query = args[1] if len(args) > 1 else None
        now = datetime.datetime.now(tz=self.localtz)
        tomorrow = now + datetime.timedelta(days=1)
        is_next, (last, name) = self.get_next_rpg_session(now, query=query)

        if is_next:
            last = last.astimezone(self.localtz)
            # S'il y a un événement prévu dans le futur
            if last < now:
                # Et qu'il a déjà commencé
                answer = "On joue maintenant ! (" + name + ")."
            elif (
                last.day == now.day
                and last.month == now.month
                and last.year == now.year
            ):
                if last.hour >= 18 or last.hour <= 3:
                    prefix = "Ce soir"
                # Et qu'il est aujourd'hui
                elif last.hour > 4 and last.hour < 12:
                    prefix = "Ce matin"
                elif last.hour > 12 and last.hour < 18:
                    prefix = "Cet après-midi"
                answer = (
                    "{prefix}".format(prefix=prefix)
                    + " on joue à {game} à {hour} ({day}) !"
                )
            elif (
                last.day == tomorrow.day
                and last.month == tomorrow.month
                and last.year == tomorrow.year
            ):
                # Et qu'il est demain
                answer = "Demain on joue à {game} à {hour} ({day}) !"
            else:
                # Et qu'il est plus tard
                answer = "On joue le {day} à {game} à {hour}."
            answer = answer.format(
                game=name, hour=last.strftime("%H:%M"), day=last.strftime("%A %d %B %Y")
            )
        else:
            answer = "Il n'y a de session de prévue :'(."
        # Envoyer la réponse sur le salon de discussion
        bot.connection.privmsg(context["channel"], answer)

    """Détermine le prochain événement du calendrier distant

    Récupère le calendrier des sessions de JdR en strftime ics et en récupère le dernier événement

    Args:
        now (datetime): horaire au moment de la requête
        query (string): chaîne de caractère devant être contenue dans le titre de l'événement

    Returns:
        bool, ical_event : Vrai si l'événement est dans le futur, événement à afficher
    """

    def get_next_rpg_session(self, now, query=None):
        ical = icalendar.Calendar.from_ical(
            requests.get(self.config["calendar_url"], timeout=10).text
        )
        events = defaultdict(list)
        for e in ical.walk():
            # Ignore non-events (e.g. timezone objects)
            if e.name != "VEVENT":
                continue
            if query and query.lower() not in e.get("summary").lower():
                continue
            # Recurrent events
            if "RRULE" in e.keys():
                e1 = get_next_from_recurrent(e, now)
                if e1 is not None:
                    events[e.get("uid").to_ical()].append(e1)
                    e2 = get_next_from_recurrent(e, e1.get("dtstart").dt)
                    if e2 is not None:
                        events[e.get("uid").to_ical()].append(e2)
            # Normal events
            else:
                events[e.get("uid").to_ical()].append(e)

        for uid, instances in events.items():
            for event in instances:
                if "RECURRENCE-ID" in event.keys():
                    rec_id = event.get("recurrence-id").to_ical()
                    events[uid] = [
                        ev
                        for ev in events[uid]
                        if ev.get("dtstart").to_ical() != rec_id
                    ]
            events[uid] = [
                ev
                for ev in events[uid]
                if ev is not None
                and ev.get("dtstart") is not None
                and fill_date(ev.get("dtstart").dt) > now
            ]
            # sorted(events[uid], key=lambda e: e.get('dtstart').dt)

        events = {k: v for k, v in events.items() if len(v) > 0}
        # candidates = [instances[-1] for _, instances in events.items()]
        candidates = []
        for k, v in events.items():
            candidates += v
        next = None
        gap = None
        for c in candidates:
            dtstart = fill_date(c.get("dtstart").dt)

            if dtstart > now and (gap is None or dtstart - now < gap):
                next = c
                gap = dtstart - now
        if next is not None:
            return (
                True,
                (next.get("dtstart").dt, next.get("summary").to_ical().decode("utf-8")),
            )
        else:
            return False, (None, None)


def get_next_from_recurrent(e, now):
    rrule = e.get("RRULE").to_ical().decode("utf-8")
    begin_date = e.get("dtstart").dt.astimezone(AgendaJdr.localtz)
    delta = e.get("dtend").dt.astimezone(AgendaJdr.localtz) - begin_date
    rule = dateutil.rrule.rrulestr(rrule, dtstart=begin_date)
    next_event = icalendar.Event()
    if "EXDATE" in e.keys():
        if not isinstance(e.get("exdate"), list):
            exceptions = [e.get("exdate")]
        else:
            exceptions = e.get("exdate")
        exceptions = [ev.dts[0].dt.date() for ev in exceptions]
    else:
        exceptions = []
    search = True
    while search:
        next = rule.after(now)
        if next is None:
            search = False
        elif next.date() not in exceptions:
            search = False
        else:
            search = True
            now = next

    if next is not None:
        # Build a new iCal event using the information
        next_event.add("uid", e.get("uid"))
        # We reuse the same dtstart and dtend, however we need to replace the timezone
        # Indeed, for daylight savings, the timezone changes but not the hour of the game
        # so we use the current tz but the original times of the first event
        next_event.add("dtstart", next.replace(tzinfo=AgendaJdr.localtz))
        next_event.add("dtend", (next + delta).replace(tzinfo=AgendaJdr.localtz))
        next_event.add("summary", e.get("summary"))
        return next_event
    else:
        return None


def fill_date(dtstart):
    if not hasattr(dtstart, "hour"):
        dtstart = datetime.datetime.combine(dtstart, datetime.datetime.min.time())
        dtstart = AgendaJdr.localtz.localize(dtstart)

    return dtstart


if __name__ == "__main__":
    jdr = AgendaJdr()
    now = datetime.datetime.now(tz=AgendaJdr.localtz)
    _, (next_time, session_name) = jdr.get_next_rpg_session(now, query="ins")
    next_time = next_time.astimezone(timezone("Europe/Paris"))
    print(
        "La prochaine session est {} à {} le {}.".format(
            session_name, next_time.strftime("%H:%M"), next_time.strftime("%A %d %B %Y")
        )
    )
