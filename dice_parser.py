import re
import random

from plugin import Plugin, PluginException


class ParseError(Exception):
    pass


class RollError(Exception):
    pass


class DiceParser(Plugin):
    """
    Plugin for basic dice rolling :
      Based on an expression XdY + Z, roll X dices (values from 1 to Y) and add Z.
      Parse the full expression in lowercase and use uniform random sampling.
    """

    # Pattern is XdY or XdY+Z
    # At most one additional space is permitted between operators
    pattern = re.compile(
        "(?P<ndice>\d+)\s?d\s?(?P<dicetype>\d+)\s?(\+\s?(?P<modifier>\d+))?"
    )

    @staticmethod
    def roll_dice(n_dices, dice_value, modifier=None):
        """
            Rolls n_dices with uniform int sampling in [1, dice_value].
            Adds modifier to all dice results (or 0 if modifier is None).
        """
        n_dices, dice_value = int(n_dices), int(dice_value)
        modifier = int(modifier) if modifier is not None else 0

        if dice_value < 1:
            raise RollError("La valeur du dé doit être plus grande que 1.")
        elif dice_value > 1000:
            raise RollError("Vraiment, tu as besoin d'un dé {} ?".format(dice_value))
        elif n_dices < 1:
            raise RollError("Le nombre de dés doit être au moins 1.")
        elif n_dices > 50:
            raise RollError("Merci de ne pas lancer plus de 50 dés à la fois.")
        else:
            return [random.randint(1, dice_value) + modifier for _ in range(n_dices)]

    @classmethod
    def parse_expression(cls, expression):
        """
            Perform full matching of the user-expression against the authorized pattern.
            Returns a dictionary of the parsed groups or raises a ParseError.
        """
        parsed_expr = re.fullmatch(cls.pattern, expression)
        if parsed_expr is not None:
            return parsed_expr.groupdict()
        else:
            raise ParseError("Unable to parse {}".format(expression))

    def __init__(self):
        help = "Roule les dés (syntaxe : XdY+Z)."
        name = "dice_parse"
        Plugin.__init__(self, name, help)
        self.commands = ["roll"]
        self.regexps = []

    def call(self, bot, context, *args):
        print(args)
        # Convert to lowercase (to ignore case)
        expression = " ".join(args[1:]).lower()
        try:
            parsed = self.parse_expression(expression)
            results = self.roll_dice(
                parsed["ndice"], parsed["dicetype"], modifier=parsed["modifier"]
            )
            answer = "Alea jacta est : {dices} (total : {total})".format(
                dices=str(results), total=sum(results)
            )
        except ParseError:
            answer = "Je ne comprends pas cette expression. :("
        except RollError as e:
            answer = str(e)
        bot.connection.privmsg(context["channel"], answer)
