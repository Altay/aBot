from plugin import Plugin


class Jingle(Plugin):
    def __init__(self):
        help = "Joue le jingle de Puchu."
        name = "jingle"
        Plugin.__init__(self, name, help)
        self.commands = ["jingle"]

    def call(self, bot, context, *args):
        answer = "🚨 ♫ La solution de psychopathe de... Puchuuuuu ! ♪ 🚨 https://cloud.whidou.fr/index.php/s/9c2KcLRatDAegL7"
        bot.connection.privmsg(context["channel"], answer)
