import logging

from plugin import Plugin, PluginException


class AdminPanel(Plugin):
    """Command center for the bot
    
    Allow to control the bot by sending it queries

    Configuration:
       password (string): password protection to forbid unauthorized commands 
    """

    def __init__(self):
        help = "Gestion et administration du bot."
        name = "admin_panel"
        Plugin.__init__(self, name, help)
        self.functions_ = {
            "say": say_,
            "join": join_,
            "part": part_,
            "quit": quit_,
            "reload": reload_,
        }
        self.commands = self.functions_.keys()
        self.password = self.config["password"]

    def call(self, bot, context, *args):
        if len(args) < 1:
            # Something went wrong
            raise PluginException
        else:
            if len(args) > 1 and args[1] == self.password:
                self.functions_[args[0]](bot, context, *args[2:])
            else:
                bot.connection.privmsg(
                    context["channel"],
                    "I'm sorry " + context["author"] + ", I'm afraid I can't do that.",
                )
                logging.warning(
                    "Invalid password from {} (tried: {})".format(
                        context["author"], args[1]
                    )
                )


def say_(bot, context, *args):
    """Makes the bot say the provided string on the specified channel

    Args:
        bot (IRC.bot): the bot to control
        context (dict): the IRC context
        args (list): destination channel, message
    """
    if len(args) > 0 and args[0].startswith("#"):
        # TODO : warning if destination is not a channel where the bot is present
        destination = args[0]
        message = " ".join(args[1:])
        bot.connection.privmsg(destination, message)
    else:
        bot.connection.privmsg(
            context["channel"], "Please specify a channel, including the '#'."
        )


def join_(bot, context, *args):
    """Makes the bot join a specified channel

    Args:
        bot (IRC.bot): the bot to control
        context (dict): the IRC context
        args (list): destination channel
    """
    # TODO : validate channel
    if args[0].startswith("#"):
        destination = args[0]
    else:
        destination = "#" + args[0]
    bot.connection.join(destination)


def part_(bot, context, *args):
    """Makes the bot part from the specified channel

    Args:
        bot (IRC.bot): the bot to control
        context (dict): the IRC context
        args (list): channel to part from
    """
    if args[0].startswith("#"):
        channel = args[0]
    else:
        channel = "#" + args[0]
    bot.connection.part(channel)


def quit_(bot, context, *args):
    """Makes the bot quit the server

    Args:
        bot (IRC.bot): the bot to control
        context (dict): the IRC context
        args (list): unused
    """
    bot.connection.privmsg(context["channel"], "Bye !")
    bot.connection.quit()
    bot.die()


def reload_(bot, context, *args):
    bot.reload_plugins()
