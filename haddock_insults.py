from random import random

from plugin import Plugin


class HaddockInsults(Plugin):
    """Haddock Plugin

    Says random insults from Captain Haddock's dictionary

    Configuration:
        filename (string): the dictionary to be used
        TODO : (opt) format (string): the format
    """

    def __init__(self):
        help = "Insultes du Capitaine Haddock"
        name = "haddock_insults"
        Plugin.__init__(self, name, help)
        self.commands = ["insulte", "haddock"]
        # TODO : configvalidation
        # TODO : add blacklist for some nicknames

    def call(self, bot, context, *args):
        if len(args) > 1:
            answer = ""
            insults = self.sample_insults()
            for nickname in args[1:]:
                answer += nickname + ", "
            answer += " ".join(insults) + " !"
            bot.connection.privmsg(context["channel"], answer)
        else:
            # TODO : raise error
            pass

    """
    Use reservoir sampling to extract a random k-subset from lines
    Reservoir sampling avoids loading the entire file in memory
    """

    def sample_insults(self, k=1):
        subset = []
        i = 0
        f = open(self.config["filename"], "r")
        for line in f:
            insult = line.strip()
            i += 1
            if len(subset) < k:
                subset.append(insult)
            else:
                p = int(random() * i)
                if p < k:
                    subset[p] = insult
        return subset
