from requests_html import HTMLSession
from web_parser.web_parser import WebParser

"""
EnProd
"""


def Prod():
    session = HTMLSession()

    help = "Est-ce qu'on met en prod aujourd'hui ?"
    name = "web_parser.prod"
    url = "http://www.estcequonmetenprodaujourdhui.info"
    commands = ["prod"]

    def parser(html):
        day = html.find("h1", first=True).text
        gif_url = url + html.find("img", first=True).attrs["src"]
        text = html.find("strong", first=True).text
        answer = " ".join([day, text, gif_url])
        answer = " ".join(answer.splitlines())
        return answer

    return WebParser(name, help, url, commands, html_parser=parser)
