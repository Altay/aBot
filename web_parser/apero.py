from web_parser.web_parser import WebParser

"""
Apéro
"""


def Apero():
    help = "Est-ce que c'est l'heure de l'apéro ?"
    name = "web_parser.apero"
    url = "http://apero.altay.fr"
    commands = ["apero", "apéro"]
    return WebParser(name, help, url, commands)
