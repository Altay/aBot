from web_parser.web_parser import WebParser

"""
Goûter
"""


def Gouter():
    help = "Est-ce que c'est l'heure du goûter ?"
    name = "web_parser.gouter"
    url = "http://gouter.altay.fr"
    commands = ["gouter", "goûter"]
    return WebParser(name, help, url, commands)
