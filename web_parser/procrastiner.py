from web_parser.web_parser import WebParser

"""
Procrastiner
"""


def Procrastiner():
    def parse_html(html):
        return common_parse(html)

    help = "Est-ce que c'est l'heure de procrastiner ?"
    name = "web_parser.procrastiner"
    url = "http://procrastiner.altay.fr"
    commands = ["procrastiner", "glander"]
    return WebParser(name, help, url, commands)
