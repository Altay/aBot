import logging
import requests
import sys

from requests import ConnectionError, Timeout
from requests_html import HTMLSession

sys.path.insert(0, "..")
from plugin import Plugin


def common_parse(html):
    hour = html.find("div#clock", first=True).text.replace("\n", "")
    message = " ".join(html.find("p", first=True).text.splitlines())
    answer = hour + " ? " + message
    return answer


"""
Generic class for the webparsers
"""


class WebParser(Plugin):
    session = HTMLSession()

    def __init__(self, name, help, url, commands, html_parser=common_parse):
        self.url = url
        Plugin.__init__(self, name, help)
        self.regexps = []
        self.commands = commands
        self.parse_html = html_parser

    def call(self, bot, context, *args):
        try:
            html = self.session.get(self.url).html
            answer = self.parse_html(html)
        except (ConnectionError, Timeout) as e:
            logging.error("Error connecting to {}".format(self.url))
            answer = "Oups, le site {} n'a pas l'air accessible…".format(self.url)
        bot.connection.privmsg(context["channel"], answer)
