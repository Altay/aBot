from web_parser.web_parser import WebParser
from bs4 import BeautifulSoup

"""
Rends l'argent François !
"""


def Argent():
    def parser(self, html):
        soup = BeautifulSoup(html, "html.parser")
        yesno = soup.find("h1").text
        text = soup.find("header").text
        answer = text + " ==> " + yesno
        answer = " ".join(answer.splitlines())
        return answer

    help = "Est-ce qu'il a rendu l'argent ?"
    name = "web_parser.argent"
    url = "https://www.estcequilarendulargent.fr/"
    commands = ["argent", "françois", "francois", "rends", "fillon"]
    return WebParser(name, help, url, commands, html_parser=parser)
