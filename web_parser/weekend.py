from web_parser.web_parser import WebParser
from bs4 import BeautifulSoup

"""
EnWeekend
"""


def Weekend():
    def parser(self, html):
        soup = BeautifulSoup(html, "html.parser")
        answer = "Est-ce qu'on est bientôt en week-end ? "
        answer += soup.find("p").text
        answer = " ".join(answer.splitlines())
        return answer

    help = "Est-ce qu'on est bientôt en week-end ?"
    name = "web_parser.weekend"
    url = "http://estcequecestbientotleweekend.fr/"
    commands = ["weekend"]
    return WebParser(name, help, url, commands, html_parser=parser)
