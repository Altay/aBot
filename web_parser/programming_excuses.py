from web_parser import WebParser
from bs4 import BeautifulSoup

"""
ProgrammerExcuses
"""


def programmerExcuses():
    def parser(html):
        soup = BeautifulSoup(html, "html.parser")
        answer = soup.find("a").text
        return answer

    help = "Programmer excuses when things go wrong"
    name = "web_parser.programmer_excuses"
    url = "http://programmingexcuses.com/"
    commands = ["ex2p"]
    return WebParser(name, help, url, commands, html_parser=parser)
