from web_parser import WebParser
from bs4 import BeautifulSoup

"""
Excuses de chercheur
"""


def scienceExcuses():
    def parser(html):
        soup = BeautifulSoup(html, "html.parser")
        answer = soup.find("a").text
        return answer

    help = "Scientists excuses when things go wrong"
    name = "web_parser.science_excuses"
    url = "http://excuses.altay.fr/"
    commands = ["ex2c"]
    return WebParser(name, help, url, commands, html_parser=parser)
