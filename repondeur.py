import json
import logging
import re
import requests

from plugin import Plugin


class Repondeur(Plugin):
    """Repondeur Plugin

    """

    def __init__(self):
        help = "Affiche des réponses automatiques"
        name = "repondeur"
        Plugin.__init__(self, name, help)
        self.mapping = {}
        self.regexps = []
        self.load_regexps_from_config()
        print(self.regexps)

    def load_regexps_from_config(self):
        for auto_answer in self.config:
            d = list(auto_answer.values())[0]
            self.regexps.append(d["regexp"])
            self.mapping[d["regexp"]] = d["message"]

    def find_correct_answer(self, message):
        for r in self.regexps:
            if len(re.findall(r, message)) > 0:
                return self.mapping[r.pattern]
        else:
            pass # TODO: add error logging here

    def call(self, bot, context, *video_ids):
        answer = self.find_correct_answer(context["message"])
        bot.connection.privmsg(context["channel"], answer)

if __name__ == "__main__":
    r = Repondeur()
    print(r.find_correct_answer("prout"))
    print(r.find_correct_answer("Héhé, coin coin \_o<"))
    print(r.find_correct_answer("Oh mais merci abot !"))
    print(r.find_correct_answer("Ah, merci aBot."))
    
