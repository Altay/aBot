import json
import logging
import requests

from plugin import Plugin
from requests import ConnectionError, Timeout
from utils import seconds_to_time


class DailymotionInfo(Plugin):
    """DailymotionInfo Plugin

    Collect automatically information about videos linked to by querying the
    Dailymotion API endpoint

    Configuration:
       dailymotion_api_url : URL of the Youtube API endpoint
    """

    def __init__(self):
        help = "Affiche automatiquement les informations des vidéos Dailymotion."
        name = "dailymotion_info"
        Plugin.__init__(self, name, help)
        self.regexps = ["https?://(?:www.)?dailymotion.com/video/([A-Za-z0-9]+)_.+"]

    def call(self, bot, context, *video_ids):
        for video_id in video_ids:
            v = self.get_dailymotion_info(video_id)
            answer = (
                "Vidéo : "
                + v["title"]
                + " ("
                + seconds_to_time(int(v["duration"]))
                + ")"
            )
            bot.connection.privmsg(context["channel"], answer)

    def get_dailymotion_info(self, video_id):
        """Returns a JSON object with video informations according to the Youtube API

        Args:
            video_id (string): id of the Youtube video
        Returns:
            object : JSON converted to a Python object
        """
        dailymotion_query = self.config["dailymotion_api_url"] + video_id
        try:
            r = requests.get(
                dailymotion_query, params={"fields": "id,title,duration"}, timeout=5
            )
            return r.json()
        except (ConnectionError, Timeout) as e:
            # Network failure or API error
            logging.error("Failure to get " + video_id + " from Dailymotion")
