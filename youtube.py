import random
import requests

from requests import ConnectionError, Timeout

from plugin import Plugin
from utils import iso8601_to_duration

class Youtube(Plugin):
    """Youtube Plugin

    Collect automatically information about videos linked to by querying the
    Google Youtube API (needs an API key)

    Configuration:
       youtube_api_videos : URL of the Youtube API endpoint
       youtube_api_key : a valid key for querying the API
    """

    def __init__(self):
        help = "Affiche automatiquement les informations d'une vidéo YouTube. La commande playlist [recherche] permet de chercher une liste de lecture selon une liste de mots-clés (par exemple: playlist hard rock)."
        name = "youtube"
        Plugin.__init__(self, name, help)
        self.commands = ["playlist"]
        self.regexps = [
            "https?://(?:www\.|m\.)?(?:youtu(?:be\.(?:com|fr)|\.be))/watch\?.*v=([\-_A-Za-z0-9]+)",
            "https?://(?:www\.|m\.)?(?:youtu\.be)/([\-_A-Za-z0-9]+)",
            "https?://(?:www\.|m\.)?(?:youtu(?:be\.(?:com|fr)|\.be))/shorts/([\-_A-Za-z0-9]+)",
        ]

    def call(self, bot, context, *args):
        if args[0] == "playlist":
            answer = self.get_playlist(args[1:])
            bot.connection.privmsg(context["channel"], answer)
        else:
            video_ids = args
            for video_id in video_ids:
                v = self.get_video_info(video_id)
                answer = "Vidéo ("+ v["channelTitle"] + ") : " + v["title"] + " (" +  v["duration"] + ")"
                bot.connection.privmsg(context["channel"], answer)

    def get_video_info(self, video_id : str) -> dict:
        """Returns a JSON object with video informations according to the Youtube API

        Args:
            video_id (string): id of the Youtube video
        Returns:
            object : JSON converted to a Python object
        """
        payload = {
            "part": "snippet,contentDetails",
            "id": video_id,
            "key": self.config["youtube_api_key"],
        }
        try:
            r = requests.get(
                self.config["youtube_api_videos"], params=payload, timeout=5
            ).json()
            video_obj = r["items"][0]["snippet"]
            video_obj["duration"] = iso8601_to_duration(r["items"][0]["contentDetails"]["duration"])
            return video_obj
        except (ConnectionError, Timeout) as e:
            # Network failure or API error
            logging.error("Failure to get {} from Youtube: {}".format(video_id, e))

    def get_playlist(self, query: str) -> str:
        print(self.config)
        # payload = {'part': 'snippet', 'key': self.config['youtube_api_key'], 'type': 'playlist', 'q': '+'.join(query),
        payload = {
            "part": "snippet",
            "type": "playlist",
            "q": " ".join(query),
            "key": self.config["youtube_api_key"],
            #"topicId": "/m/04rlf",  # topic id for the "Music" category"
            "order": "relevance",
            "maxResults": 20,
        }
        try:
            r = requests.get(
                self.config["youtube_api_search"], params=payload, timeout=5
            )
            playlists = r.json()["items"]
            if len(playlists) == 0:
                return "Aucune playlist correspondante n'a été trouvée. :("
            #playlist = random.sample(playlists, 1)[0]
            playlist = playlists[0]
            playlist_id = playlist["id"]["playlistId"]
            playlist_title = playlist["snippet"]["title"]
            payload = {
                "part": "snippet",
                "playlistId": playlist_id,
                "key": self.config["youtube_api_key"],
            }
            r = requests.get(
                self.config["youtube_api_playlists"], params=payload, timeout=5
            )
            first_video = r.json()["items"][0]["snippet"]["resourceId"]["videoId"]
            playlist_url = "https://www.youtube.com/watch?v={video_id}&list={playlist_id}".format(
                video_id=first_video, playlist_id=playlist_id
            )
            return "Playlist : {url} ({title})".format(
                url=playlist_url, title=playlist_title
            )
        except (ConnectionError, Timeout) as e:
            # Network failure or API error
            logging.error("Failure to get Youtube playlist: {}".format(e))

if __name__ == "__main__":
    youtube = Youtube()

    print(youtube.get_playlist(["chez", "marcus", "nolife"]))
    print(youtube.get_video_info("XrVp6hlePfk"))
    print(youtube.get_video_info("B5ZPzK5tT0g")) # shorts
