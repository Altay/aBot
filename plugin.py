import logging
import sys
import traceback
import yaml

from abc import ABC, abstractmethod


class PluginException(Exception):
    pass


class Plugin(ABC):
    """
    name : le nom du module (unique)
    commands : différents alias pour appeler ce module
    help : texte de documentation
    """

    def __init__(self, name, help):
        self.name = name
        self.commands = []
        self.regexps = []
        self.help = help
        conf = yaml.safe_load(open("config.yml"))
        self.config = conf.get(self.name, {})

    def process(self, bot, context, *args):
        try:
            self.call(bot, context, *args)
            logging.debug("Invoke plugin {} using args: {}".format(self.name, args))
        except PluginException as e:
            logging.warn("Error in plugin {} (called with args {}): {}.".format(self.name, args, e))
        except:
            logging.error("Unexpected error in module {}:".format(self.name), *sys.exc_info())
            traceback.print_exc()

    @abstractmethod
    def call(self, bot, context, *args):
        # Should be specialized
        raise NotImplementedError
