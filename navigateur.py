import json
import re
import random
import requests

from bs4 import BeautifulSoup

from plugin import Plugin
from plugin import Plugin, PluginException


def populate_list(url):
    html = requests.get(url, timeout=10).text
    soup = BeautifulSoup(html, "html.parser")
    div = soup.find("div", {"class": "mw-parser-output"})
    explorers = []
    # Hack
    all_li = div.find_all("li")[:-10]
    for li in all_li:
        links = li.find_all("a")
        for link in links:
            if link.find("img") is None:
                explorers.append((link.text, "https://fr.wikipedia.org" + link["href"]))
                break
    return explorers


class Navigateur(Plugin):
    def __init__(self):
        help = "Se plaint des navigateurs célèbres."
        name = "navigateur"
        Plugin.__init__(self, name, help)
        self.commands = []
        self.regexps = [
            ".*(([Pp]utain)|([Ff]uck)|([Bb]ordel)|([Mm]erde)|([Cc]ouilles?)).*[Nn]avigateur.*",
            ".*[Nn]avigateur.*(([Pp]utain)|([Ff]uck)|([Bb]ordel)|([Mm]erde)|([Cc]ouilles?)).*",
        ]
        self.authors = ["puchu", "Puchu", "puchu[m]", "Puchu[m]", "Altay"]
        url = self.config["url"]
        self.explorers = populate_list(url)

    def call(self, bot, context, *args):
        if context["author"] in self.authors:
            explorer = random.sample(self.explorers, 1)[0]
            answer = "୧( ಠ Д ಠ )୨  {name} ! ({link})".format(
                name=explorer[0], link=explorer[1]
            )
            bot.connection.privmsg(context["channel"], answer)


if __name__ == "__main__":
    url = "https://fr.wikipedia.org/wiki/Liste_alphab%C3%A9tique_de_marins"
    l = populate_list(url)
    print(l)
    explorer = random.sample(l, 1)[0]
    print(explorer)
    print(explorer[0])
    print(explorer[1])
