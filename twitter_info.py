import logging
import re
import requests

from dataclasses import dataclass
from enum import Enum
from requests_html import HTMLSession
from requests import ConnectionError, Timeout
from plugin import Plugin, PluginException
from utils import retry


class Mode(Enum):
    API = "API"
    HTML = "HTML"
    NITTER = "Nitter"


@dataclass
class Tweet:
    author: str = ""
    text: str = ""


class TwitterInfo(Plugin):
    """ Extract the username and tweet content based on
        a Twitter status URL.

        This plugin supports three parsing modes:
        - API [deprecated]: used the v1 Twitter API, not supported anymore
        - HTML: parses the raw HTML from Twitter web.
        - Nitter [preferred]: parses the raw HTML from the alternative Nitter frontend.
        """
    accepted_modes = [mode.value for mode in Mode]

    def __init__(self, mode : Mode=None):
        """ Args:
                mode (optional): API, HTML or NITTER
        """
        help = "Affiche automatiquement le contenu d'un tweet."
        name = "twitter_info"
        Plugin.__init__(self, name, help)
        self.regexps = [
            "https?://(?:www.)?(?:mobile.)?twitter.com/[A-Za-z0-9\-_]+/status/[0-9]+"
        ]
        self.mode = mode if mode is not None else Mode.NITTER
        if self.mode.value not in self.accepted_modes:
            raise ValueError(
                "Unknown mode {} for TwitterInfo. Accepted modes are: {}".format(
                    self.mode, ", ".join(self.accepted_modes)
                )
            )
        self.session = HTMLSession()

    def call(self, bot, context, *tweet_ids) -> None:
        """ Args:
            - tweet_ids: positional arguments of tweet URLs

            Sends a message to the channel containing the content of the tweet.
        """
        for tweet_id in tweet_ids:
            # Workaround fo mobile URLs
            tweet_id = tweet_id.replace("mobile.twitter.com", "twitter.com")
            # Get tweet information based on the correct mode
            match self.mode:
                case Mode.API:
                    t = self.get_api_twitter_info(tweet_id)
                case Mode.HTML:
                    t = self.get_html_twitter_info(tweet_id)
                case Mode.NITTER:
                    t = self.get_nitter_twitter_info(tweet_id)
            # Format the answer for sending to chat
            answer = f'{t.author} : {t.text}'
            answer = " ".join(answer.splitlines())
            answer = answer.replace("http", " http")
            nitter_link = tweet_id.replace("twitter.com", "nitter.tux.pizza")
            answer += f" (lien Nitter: {nitter_link})"
            bot.connection.privmsg(context["channel"], answer)

    def get_api_twitter_info(self, tweet_url : str) -> Tweet:
        """
            [deprecated]
            Query the Twitter API v1 to get the username and the content of the status.
            As of 2023/04, does not work anymore (thanks Leon Murk).
        """
        tweet_id = tweet_url.split("/")[-1]
        payload = {
            "id": tweet_id,
            "tweet_mode": "extended",  # Necessary, otherwise Twitter API answers with truncated tweet
        }
        headers = {"Authorization": "Bearer {}".format(self.config["bearer_token"])}
        try:
            r = requests.get(
                self.config["twitter_api_url"],
                params=payload,
                headers=headers,
                timeout=5,
            )
            print(r)
            r = r.json()
            return Tweet(text=r["full_text"], author=r["user"]["name"] + " (@" + r["user"]["screen_name"]+ ")")
        except (ConnectionError, Timeout) as e:
            logging.warning("Error connecting to Twitter: {}".format(e))
            return None

    def get_html_twitter_info(self, tweet_url : str) -> Tweet:
        """
            Request the HTML from Twitter web client and parse the status page
            to extract the username and content of the tweet.

            As of 2023/04, does not work anymore as the HTML is JS-rendered.
        """
        try:
            tweet_obj = Tweet()
            r = self.session.get(tweet_url, timeout=5)
            tweet_div = r.html.find("div.permalink-tweet", first=True)
            tweet_obj.author = tweet_div.find("span.username", first=True).text
            tweet_obj.text = tweet_div.find("p.tweet-text", first=True).text
            tweet_obj.text = tweet_obj.text.replace(
                "pic.twitter.com", "https://pic.twitter.com"
            )
            tweet_obj.text = tweet_obj.text.replace("#\n", "#")
            tweet_obj.text = tweet_obj.text.replace("http", " http")
            return tweet_obj
        except (ConnectionError, Timeout) as e:
            logging.warning("Error connecting to Twitter: {}".format(e))
        except AttributeError as e:
            logging.warning("Probable error when parsing Twitter HTML: {}".format(e))
            raise e

    def get_nitter_twitter_info(self, tweet_url : str) -> Tweet:
        """
            Request the HTML from the Nitter alternative frontend and parse the
            status page to extract the username and content of the tweet.

            Preferred method since 2023/04.
        """
        try:
            tweet_obj = Tweet()
            tweet_url = tweet_url.replace("twitter.com", self.config["nitter_instance"])
            r = self.session.get(tweet_url, timeout=5)
            tweet_div = r.html.find("div.main-tweet", first=True)
            tweet_obj.author = tweet_div.find("a.fullname", first=True).text
            tweet_obj.author += f' ({tweet_div.find("a.username", first=True).text})'
            tweet_obj.text = tweet_div.find("div.tweet-content", first=True).text
            tweet_obj.text = tweet_obj.text.replace("http", " http")
            return tweet_obj
        except (ConnectionError, Timeout) as e:
            logging.warning(f"Error connecting to {self.config['nitter_instance']}: {e}")
        except AttributeError as e:
            logging.warning(f"Probable error when parsing Nitter HTML: {e}")
            raise e


if __name__ == "__main__":
    twitter_info = TwitterInfo()
    t = twitter_info.get_nitter_twitter_info(
        #"https://twitter.com/leroncier/status/999252902538510336"
        "https://twitter.com/ConneriesSurVHS/status/1672700674482786304"
    )
    answer = t.author + ": " + t.text
    answer = " ".join(answer.splitlines())
    print(answer)
