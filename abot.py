import argparse
import irc.bot
import importlib
import logging
import re
import sys
import utils
import yaml

from jaraco.functools import Throttler

# Try to fallback to ISO-8859 when UTF-8 fails
from jaraco.stream import buffer

irc.client.ServerConnection.buffer_class = buffer.LenientDecodingLineBuffer

parser = argparse.ArgumentParser(
    description="An IRC bot that does stuff using plugins."
)
parser.add_argument(
    "--conf", default="config.yml", type=str, help="Path to configuration file"
)
server_args = parser.add_argument_group("server")
server_args.add_argument(
    "--url", default=None, type=str, help="Server adress to connect"
)
server_args.add_argument("--port", default=6667, type=int, help="IRC port")
server_args.add_argument(
    "--channels", default=None, nargs="+", help="List of channels prefixed by #"
)
bot_args = parser.add_argument_group("bot")
bot_args.add_argument("--nick", default="aBot", type=str, help="Bot nickname")
bot_args.add_argument("--realname", default="aBot", type=str, help="Bot realname")

logging.basicConfig(stream=sys.stdout, level=logging.INFO)


class ABot(irc.bot.SingleServerIRCBot):
    def reload_plugins(self):
        """ Drop all currently loaded plugins and reload based on the config file. """
        # Load the configuration file
        config = self.read_configuration_file()
        plugins = config["plugins"]

        # Get the name for each plugin
        current_plugins = [m.__name__ for _, m in self.loaded_plugins.items()]
        # Compute the new plugins that we have to import
        new_plugins = list(set(plugins) - set(current_plugins))
        # Find the old plugins that only need to be reloaded
        old_plugins = list(set(current_plugins).intersection(set(plugins)))

        # Recreate the plugins and import the new ones
        self.load_plugins(plugins, import_modules=new_plugins, reload_old=old_plugins)

    def load_plugins(self, plugins, import_modules=[], reload_old=None):
        """ Load plugins into the bot by importing them if needed. 
        Args :
            plugins : list of plugin names (same as the config file)
            import_modules : names of the modules we have to import in Python
            reload_old : set to True if we want to re-import the old modules
        """
        if reload_old is not None:
            # Reload already loaded modules
            logging.debug("---- Old plugins -----")
            logging.debug(reload_old)
            logging.debug("Currently loaded :")
            logging.debug(self.loaded_plugins)
            for _, module in [
                p for p in self.loaded_plugins.items() if p[0] in reload_old
            ]:
                logging.debug("Old module reloaded :")
                logging.debug(_)
                importlib.reload(module)

        old_plugins = self.plugins
        # Reset the plugins attribute
        self.plugins = []

        # Import the modules in Python using importlib
        for module_name in import_modules:
            module = importlib.import_module(module_name)
            self.loaded_plugins[module_name] = module
            logging.info(module_name + " loaded")
        # Instantiate the Module object
        for plugin_name in plugins:
            plugin = self.loaded_plugins[plugin_name]
            # The class name is derived by switching to CamelCase
            plugin_class_name = utils.snake_to_camel_case(plugin.__name__).split(".")[
                -1
            ]
            plugin_class = getattr(plugin, plugin_class_name)
            self.plugins.append(plugin_class())
        logging.info(self.plugins)

        # Pre-compile the regexps
        for plugin in self.plugins:
            plugin.regexps = [re.compile(reg) for reg in plugin.regexps]

    def get_plugin(self, plugin_name):
        for p in self.plugins:
            if p.name == plugin_name:
                return p

    def read_configuration_file(self):
        """ Load the Yaml configuration file based on the filename. """
        with open(self.configuration_file, "r") as f:
            config = yaml.safe_load(f)
        return config

    def __init__(self, configuration_file=None, **config):
        """ Init the bot by reading the configuration and setting the parameters. """
        self.configuration_file = configuration_file
        # Extra arguments manually passed are overridden by the conf file
        if self.configuration_file is not None:
            config.update(self.read_configuration_file())

        # Imported plugins
        self.loaded_plugins = {}
        # Instances of loaded Module objects
        self.plugins = []
        # Let's import the modules in Python and instantiate the plugins
        self.load_plugins(config["plugins"], import_modules=config["plugins"])

        # Set the connection parameters
        server_url = config["server"].get("url", None)
        server_port = config["server"].get("port", 6667)
        self.nickname = config["bot"].get("nickname", "a_weird_bot")
        self.realname = config["bot"].get("realname", "A weird bot")
        self.start_channels = config["server"].get("channels", [])

        logging.info(f"Connecting as {self.nickname} to {server_url}:{server_port}")

        connect_params = {}
        if sasl := config["bot"].get("sasl", None):
            connect_params["sasl_login"] = sasl

        if password := config["server"].get("password", None):
            connect_params["password"] = password

        logging.debug(connect_params)

        # Start the bot
        irc.bot.SingleServerIRCBot.__init__(
            self, [(server_url, server_port)], self.nickname, self.realname, connect_params
        )

        logging.info("Connected!")

        # Set the global throttling
        global_throttle = config.get("throttle", {}).get("global", None)
        if global_throttle and global_throttle > 0:
            self.connection.set_rate_limit(global_throttle)
        # Throttle messaging to n messages/second to avoid kicks and bans
        messaging_throttle = config.get("throttle", {}).get("messages", 0)
        if messaging_throttle > 0 and messaging_throttle:
            self.connection.privmsg = Throttler(
                self.connection.privmsg, messaging_throttle
            )

    def on_welcome(self, server, event):
        """ Join the channels after the connection. """
        logging.info("Welcome message received")
        for channel in self.start_channels:
            server.join(channel)

    def on_pubmsg(self, server, event):
        """ Parse the public messages that are received. """
        context = {
            "author": event.source.nick,
            "channel": event.target,
            "message": event.arguments[0],
        }
        self.process_msg(context)

    def on_privmsg(self, server, event):
        """ Parse the private messages that are received. """
        context = {
            "author": event.source.nick,
            "channel": event.source.nick,
            "message": event.arguments[0],
        }
        self.process_msg(context)

    def process_msg(self, context):
        """ Process a message, either by matching a regexp or by finding a specific command. """
        for plugin in self.plugins:
            for command in plugin.commands:
                # Commands follow the syntax "!ab command"
                if context["message"].startswith("!ab " + command):
                    # Pass the args in a UNIX way (args[0] is the command name, etc.)
                    plugin.process(self, context, *context["message"].split(" ")[1:])
            for regexp in plugin.regexps:
                # Try to match a regexp
                args = regexp.findall(context["message"])
                if args:
                    plugin.process(self, context, *args)
                    break


if __name__ == "__main__":
    args = parser.parse_args()
    user_args = {
        "server": {"url": args.url, "port": args.port, "channels": args.channels},
        "bot": {"nickname": args.nick, "realname": args.realname},
    }

    bot = ABot(configuration_file=args.conf, **user_args)
    # Start the bot
    bot.start()
