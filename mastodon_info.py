import json
import logging
import re
import requests
from requests import ConnectionError, Timeout
from requests_html import HTMLSession

from dataclasses import dataclass
from plugin import Plugin, PluginException
from bs4 import BeautifulSoup
import html


@dataclass
class Toot:
    author : str = ""
    text : str = ""
    media: str = None

class MastodonInfo(Plugin):
    """ Extracts username and content of a Mastodon toot, then formats it and sends it to chat.
        This plugin supports most of Mastodon instances, even those that have disabled the API.
        However, unlisted instances might simply not be detected if they do not appear on instances.social.
    """
    def __init__(self):
        help = "Info from Mastodon"
        name = "mastodon_info"
        Plugin.__init__(self, name, help)
        bots_instances = ["botsin.space"]
        self.regexps = [f"https?://{domain}/@.+/\d+" for domain in self.get_instance_list_() + bots_instances]

    def get_instance_list_(self) -> list[str]:
        """
            Find a list of main URL of Mastodon instances. All found Mastodon instances
            will be added to the regexp list, so that the plugin knows the URL might
            point to a Mastodon toot.

            The instance list of based on the https://instances.social/ API.
        """
        logging.info(f"Extracting list of Mastodon instances from {self.config['instances_social_api_url']}")
        payload = {
            "count": 0,
        }
        headers = {"Authorization": "Bearer {}".format(self.config["instances_social_api_secret"])}
        logging.info(f'Getting Mastodon instance list from {self.config["instances_social_api_url"]}')
        try:
            r = requests.get(
                self.config["instances_social_api_url"],
                params=payload,
                headers=headers,
                timeout=5,
            )
            r = r.json()
            logging.info(f"Found {len(r['instances'])} instances")
            return [instance['name'] for instance in r['instances']]
        except (ConnectionError, Timeout) as e:
            logging.warning("Error connecting to instances.social: {}".format(e))
            return None


    def call(self, bot, context, *urls) -> None:
        for url in urls:
            toot = self.get_mastodon_info(url)
            if toot is not None:
                answer = f"{toot.author} :  {toot.text}"
                for media_url in toot.media:
                    answer += " " + media_url
                answer = " ".join(answer.splitlines())
                answer = answer.replace("http", " http")
                bot.connection.privmsg(context["channel"], answer)

    def get_mastodon_info(self, toot_url : str) -> Toot:
        """ Extract the username and content of a status (~toot) from a Mastodon instance.

            This method queries by default the Mastodon API on the target instance. If it
            is not available, then it falls back to HTML parsing.
        """
        try:
            url_parts = toot_url.split("/")
            masto_domain, toot_id = "/".join(url_parts[:3]), url_parts[-1]
            masto_api_url = f"{masto_domain}/api/v1/statuses/{toot_id}/"
            try:
                r = requests.get(masto_api_url, timeout=5)
            except (ConnectionError, Timeout) as e:
                logging.warning(f"Error connecting to {masto_domain}: {e}")

            # If HTTP code is not 200 (OK), then fallback to HTML parser
            if r.status_code == 200:
                r = r.json()
                author, displayname = r["account"]["username"], r["account"]["display_name"]
                toot = Toot(
                    author = f"{displayname} ({author})",
                    text   = BeautifulSoup(r["content"], features="html.parser").get_text(),
                    media  = [attachment["url"] for attachment in r["media_attachments"]],
                )
            else:
                logging.warning(f"Falling back to HTML Mastodon parsing for {toot_url}")
                toot = self.get_mastodon_info_html_fallback(toot_url)
            return toot
        except Exception as e:
            # TODO
            logging.error(e)
            raise PluginException

    def get_mastodon_info_html_fallback(self, toot_url : str) -> Toot:
        """ Extract the username and content from a Mastodon toot by parsing the HTML page.
        """
        r = HTMLSession().get(toot_url, timeout=5)
        attachments = r.html.xpath('//meta[@property="og:image"]/@content')
        toot = Toot(
                author = r.html.xpath('//meta[@property="og:title"]/@content')[0],
                text   = r.html.xpath('//meta[@property="og:description"]/@content')[0],
                media  = [media_url for media_url in attachments if "avatars" not in media_url],
        )
        return toot


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    mastodon = MastodonInfo()

    urls = [
        #"https://xoxo.zone/@qristy/108194962604365151",
        #"https://octodon.social/@OneFluffyBoi/108024906488349979",
        #"https://mastodon.social/@Altay/102408035078214960",
        #"https://octodon.social/@Lomig/108198088355353041",
        #"https://mastodon.social/@Altay/108197604862622793",
        #"https://botsin.space/@hourlyfoxbot/108198561373004534",
        "https://mastodon.social/@KamuroFriday/110909797909565076",
    ]
    for url in urls:
        try:
            print(mastodon.get_mastodon_info(url))
        except Exception as e:
            print("Erreur:", e)
